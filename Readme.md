# How to build:

1. git clone https://iaggarwal@bitbucket.org/iaggarwal/quantcastcli.git
2. cd QuantcastCLI
3. mvn clean install

This should have generated a jar file with name - QuantcastCLI-1.0-SNAPSHOT.jar

***

## Usage

```
Usage: <main class> [-d=<date>] [-f=<filePath>]
  -d, --date=<date>       Date for which max occurrence of same cookie is to be
                            calculated. Date should be in format YYYY-MM-DD
  -f, --file=<filePath>   Path for cookie file(.csv format)
```

***


## How to run
`java -jar ./target/QuantcastCLI-1.0-SNAPSHOT.jar -f "<file_path>" -d "<date>"`

***

## How to run test
`mvn clean test`