package org.ishan.quantcast;

import org.ishan.quantcast.services.CookieFileService;
import org.ishan.quantcast.services.HelperService;
import org.ishan.quantcast.services.UtilitiesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import picocli.CommandLine;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
class CookieFileCLITest {

	@Mock
	Validator mockValidator;
	@Spy
	Set<ConstraintViolation<QuantcastCLI>> mockViolations;
	@Mock
	ConstraintViolation<QuantcastCLI> cv1;
	@Mock
	CommandLine.Model.CommandSpec spec;
	@Mock
	CommandLine cl;
	@Spy
	HashSet<String> mockSet;

	MockedStatic<CookieFileService> msCfs;
	MockedStatic<UtilitiesService> msUs;
	CookieFileCLI cfCli;

	@BeforeEach
	void setup() {
		msUs = Mockito.mockStatic(UtilitiesService.class);
		msCfs = Mockito.mockStatic(CookieFileService.class);
		mockViolations.add(cv1);
		msUs.when(UtilitiesService::getValidator).thenReturn(mockValidator);
		msCfs.when(() -> CookieFileService.getCookiesWithMaxOccurrences(Mockito.anyString(), Mockito.anyString())).thenReturn(mockSet);
		cfCli = Mockito.spy(new CookieFileCLI());
	}

	@AfterEach
	void close() {
		msUs.close();
		msCfs.close();
	}

	@Test
	void validatorTest() {
		Mockito.when(mockValidator.validate(Mockito.any(QuantcastCLI.class))).thenReturn(mockViolations);
		Mockito.when(mockViolations.isEmpty()).thenReturn(true);
		Assertions.assertTrue(cfCli.validate(spec));
	}

	@Test
	void validatorTest_violations() {
		Mockito.when(spec.commandLine()).thenReturn(cl);
		Set<ConstraintViolation<QuantcastCLI>> cvQcli = new HashSet<>();
		cvQcli.add(cv1);
		Mockito.when(mockValidator.validate(Mockito.any(QuantcastCLI.class))).thenReturn(cvQcli);
		Mockito.when(cv1.getMessage()).thenReturn("Ishan failed!");
		Assertions.assertThrows(CommandLine.ParameterException.class,
				() -> cfCli.validate(spec),
				"ERROR: Ishan failed!\n");
	}

	@Test
	void callTest() {
		cfCli.filePath = "some/path/";
		cfCli.date = "2018-09-05";
		Assertions.assertNull(cfCli.call());
		Mockito.verify(cfCli, Mockito.times(1)).validate(Mockito.any());
		msCfs.verify(Mockito.times(1), () -> CookieFileService.getCookiesWithMaxOccurrences(cfCli.filePath, cfCli.date));
	}
}
