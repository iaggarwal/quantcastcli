package org.ishan.quantcast.validators;

import org.ishan.quantcast.services.UtilitiesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;
import java.io.File;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateValidatorTest {
	DateValidator dv;

	@Mock
	ConstraintValidatorContext cvc;

	MockedStatic<UtilitiesService> msUs;

	@BeforeEach
	void setup() {
		msUs = Mockito.mockStatic(UtilitiesService.class);
		msUs.when(() -> UtilitiesService.getParsedLocalDate(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenCallRealMethod();
		dv = new DateValidator();
	}

	@AfterEach
	void close() {
		msUs.close();
	}

	@Test
	void testNullEmpty() {
		Assertions.assertTrue(dv.isValid(null, cvc));
		Assertions.assertTrue(dv.isValid("", cvc));
	}

	@Test
	void testValidDate() {
		Assertions.assertTrue(dv.isValid("2018-01-01", cvc));
	}

	@Test
	void testInValidDate() {
		Assertions.assertFalse(dv.isValid("01-01-2018", cvc));
		Assertions.assertFalse(dv.isValid("2018-01-01 10:10:10", cvc));
	}

	@Test
	void testInValidDate_Exception() {
		msUs.when(() -> UtilitiesService.getParsedLocalDate(ArgumentMatchers.anyString(),
				ArgumentMatchers.any(DateTimeFormatter.class))).thenThrow(DateTimeParseException.class);
		Assertions.assertFalse(dv.isValid("01-01-2018", cvc));
	}
}
