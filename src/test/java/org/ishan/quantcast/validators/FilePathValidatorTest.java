package org.ishan.quantcast.validators;

import org.ishan.quantcast.services.UtilitiesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;
import java.io.File;

@ExtendWith(MockitoExtension.class)
class FilePathValidatorTest {

	FilePathValidator fpv;

	@Mock
	ConstraintValidatorContext cvc;
	@Mock
	File mockFile;

	MockedStatic<UtilitiesService> msUs;

	@BeforeEach
	void setup() {
		fpv = new FilePathValidator();
		msUs = Mockito.mockStatic(UtilitiesService.class);
		msUs.when(() -> UtilitiesService.getFile(ArgumentMatchers.anyString())).thenReturn(mockFile);
	}

	@AfterEach
	void close() {
		msUs.close();
	}

	@Test
	void testNullEmpty() {
		Assertions.assertTrue(fpv.isValid(null, cvc));
		Assertions.assertTrue(fpv.isValid("", cvc));
	}

	@Test
	void testFileDoesNotExist() {
		Mockito.when(mockFile.exists()).thenReturn(false);
		Assertions.assertFalse(fpv.isValid("/some/path/", cvc));
	}

	@Test
	void testFileExists() {
		Mockito.when(mockFile.exists()).thenReturn(true);
		Assertions.assertTrue(fpv.isValid("/some/path/test.csv", cvc));
	}

	@Test
	void testFileInvalidExtension() {
		Mockito.when(mockFile.exists()).thenReturn(true);
		Assertions.assertFalse(fpv.isValid("/some/path/test.exe", cvc));
	}
}
