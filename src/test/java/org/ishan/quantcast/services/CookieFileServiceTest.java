package org.ishan.quantcast.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;

public class CookieFileServiceTest {
	List<String> fileContent;
	MockedStatic<UtilitiesService> msUs;
	private final String l2 ="AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00";
	private final String l3 ="SAZuXPGUrfbcn5UA,2018-12-09T10:13:00+00:00";
	private final String l4 ="5UAVanZf6UtGyKVS,2018-12-09T07:25:00+00:00";
	private final String l5 ="AtY0laUfhglK3lC7,2018-12-09T06:19:00+00:00";
	private final String l6 ="SAZuXPGUrfbcn5UA,2018-12-08T22:03:00+00:00";
	private final String l7 ="4sMM2LxV07bPJzwf,2018-12-08T21:30:00+00:00";
	private final String l8 ="fbcn5UAVanZf6UtG,2018-12-08T09:30:00+00:00";
	private final String l9 ="4sMM2LxV07bPJzwf,2018-12-07T23:30:00+00:00";

	@AfterEach
	public void close() {
		msUs.close();
		fileContent = null;
	}

	@BeforeEach
	public void setup() {
		fileContent = new ArrayList<>();
		fileContent.add(l2);
		fileContent.add(l3);
		fileContent.add(l4);
		fileContent.add(l5);
		fileContent.add(l6);
		fileContent.add(l7);
		fileContent.add(l8);
		fileContent.add(l9);
		fileContent.add("4sMM2LxV07bPJzwf_2,2018-12-05T23:30:00+00:00");
		fileContent.add("4sMM2LxV07bPJzwf_1,2018-12-05T23:30:00+00:00");
		fileContent.add("4sMM2LxV07bPJzwf_2,2018-12-05T23:30:00+00:00");
		fileContent.add("4sMM2LxV07bPJzwf_1,2018-12-05T23:30:00+00:00");
		fileContent.add("4sMM2LxV07bPJzwf_1,2018-12-05T23:30:00+00:00");
		fileContent.add("4sMM2LxV07bPJzwf_2,2018-12-05T23:30:00+00:00");
		msUs = Mockito.mockStatic(UtilitiesService.class);
		msUs.when(() -> UtilitiesService.getFileLines(anyString()))
				.thenAnswer((Answer<Stream<String>>) invocationOnMock -> fileContent.stream());
		msUs.when(() -> UtilitiesService.getParsedLocalDateTime(anyString(), any(DateTimeFormatter.class))).thenCallRealMethod();
		msUs.when(() -> UtilitiesService.getParsedLocalDate(anyString(), any(DateTimeFormatter.class))).thenCallRealMethod();
	}

	@Test
	void getCookiesWithMaxOccurrencesTest_success() {
		Set<String> expected = new HashSet<>();
		expected.add("SAZuXPGUrfbcn5UA");
		expected.add("4sMM2LxV07bPJzwf");
		expected.add("fbcn5UAVanZf6UtG");
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-08"));

		expected = new HashSet<>();
		expected.add("4sMM2LxV07bPJzwf");
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-07"));

		expected = new HashSet<>();
		expected.add("AtY0laUfhglK3lC7");
		expected.add("SAZuXPGUrfbcn5UA");
		expected.add("5UAVanZf6UtGyKVS");
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-09"));

		expected = new HashSet<>();
		expected.add("4sMM2LxV07bPJzwf_2");
		expected.add("4sMM2LxV07bPJzwf_1");
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-05"));
	}

	@Test
	void getCookiesWithMaxOccurrencesTest_no_result() {
		Set<String> expected = new HashSet<>();
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-06"));
	}

	@Test
	void getCookiesWithMaxOccurrencesTest_Outside_Boundary() {
		Set<String> expected = new HashSet<>();
		Assertions.assertEquals(expected, CookieFileService.getCookiesWithMaxOccurrences("some/path/", "2018-12-06"));
	}
}
