package org.ishan.quantcast.services;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockedStatic;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;


class HelperServiceTest {

	List<String> fileContent;
	MockedStatic<UtilitiesService> msUs;
	private final String l1 ="cookie,timestamp";
	private final String l2 ="AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00";
	private final String l3 ="SAZuXPGUrfbcn5UA,2018-12-09T10:13:00+00:00";
	private final String l4 ="5UAVanZf6UtGyKVS,2018-12-09T07:25:00+00:00";
	private final String l5 ="AtY0laUfhglK3lC7,2018-12-09T06:19:00+00:00";
	private final String l6 ="SAZuXPGUrfbcn5UA,2018-12-08T22:03:00+00:00";
	private final String l7 ="4sMM2LxV07bPJzwf,2018-12-08T21:30:00+00:00";
	private final String l8 ="fbcn5UAVanZf6UtG,2018-12-08T09:30:00+00:00";
	private final String l9 ="4sMM2LxV07bPJzwf,2018-12-07T23:30:00+00:00";

	@AfterEach
	public void close() {
		msUs.close();
		fileContent = null;
	}

	@BeforeEach
	public void setup() {
		fileContent = new ArrayList<>();
		fileContent.add(l1);
		fileContent.add(l2);
		fileContent.add(l3);
		fileContent.add(l4);
		fileContent.add(l5);
		fileContent.add(l6);
		fileContent.add(l7);
		fileContent.add(l8);
		fileContent.add(l9);
		msUs = Mockito.mockStatic(UtilitiesService.class);
		msUs.when(() -> UtilitiesService.getFileLines(anyString()))
				.thenAnswer((Answer<Stream<String>>) invocationOnMock -> fileContent.stream());
		msUs.when(() -> UtilitiesService.getParsedLocalDateTime(anyString(), any(DateTimeFormatter.class))).thenCallRealMethod();
	}

	@Test
	void isValidLineTest() {
		Assertions.assertTrue(HelperService.isValidLine("a,b"));
		Assertions.assertFalse(HelperService.isValidLine(null));
		Assertions.assertFalse(HelperService.isValidLine(""));
		Assertions.assertFalse(HelperService.isValidLine("a,b,c"));
	}

	@Test
	void getNthLineFromFileTest() {
		Assertions.assertEquals(l2, HelperService.getNthLineFromFile("some/path", true, 1));
		Assertions.assertEquals(l4, HelperService.getNthLineFromFile("some/path", true, 3));
		Assertions.assertEquals("", HelperService.getNthLineFromFile("some/path", true, 10));
		fileContent.add(1, "");
		Assertions.assertEquals(l2, HelperService.getNthLineFromFile("some/path", true, 1));
		fileContent.add(1, "e,b,2");
		Assertions.assertEquals(l2, HelperService.getNthLineFromFile("some/path", true, 1));
	}

	@Test
	void getNumberOfLinesInFileTest() {
		Assertions.assertEquals(fileContent.size(), HelperService.getNumberOfLinesInFile("some/path", false));
		Assertions.assertEquals(fileContent.size() - 1, HelperService.getNumberOfLinesInFile("some/path", true));
	}

	@Test
	void getDateFromLineTest() {
		LocalDateTime expected = UtilitiesService.getParsedLocalDateTime("2018-12-09T14:19:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		Assertions.assertEquals(expected.toLocalDate(), HelperService.getDateFromLine(l2).get());
		Assertions.assertEquals(Optional.empty(), HelperService.getDateFromLine(l1));
		Assertions.assertEquals(Optional.empty(), HelperService.getDateFromLine(null));
		Assertions.assertEquals(Optional.empty(), HelperService.getDateFromLine(""));
		Assertions.assertEquals(Optional.empty(), HelperService.getDateFromLine("a,b,c"));
	}

	@Test
	void isLineDateSameAsInputDateTest() {
		LocalDate inputDate = UtilitiesService.getParsedLocalDateTime("2018-12-09T14:19:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertTrue(HelperService.isLineDateSameAsInputDate(l2, inputDate));
		Assertions.assertFalse(HelperService.isLineDateSameAsInputDate(l9, inputDate));
		Assertions.assertFalse(HelperService.isLineDateSameAsInputDate(l1, inputDate));
	}

	@Test
	void isLineDateGreaterThanInputDateTest() {
		LocalDate inputDate = UtilitiesService.getParsedLocalDateTime("2018-12-08T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertTrue(HelperService.isLineDateGreaterThanInputDate(l2, inputDate));
		Assertions.assertFalse(HelperService.isLineDateGreaterThanInputDate(l9, inputDate));
		Assertions.assertFalse(HelperService.isLineDateGreaterThanInputDate(l1, inputDate));
	}

	@Test
	void isLineDateLessThanOrEqualToInputDateTest() {
		LocalDate inputDate = UtilitiesService.getParsedLocalDateTime("2018-12-08T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertFalse(HelperService.isLineDateLessThanOrEqualToInputDate(l2, inputDate));
		Assertions.assertTrue(HelperService.isLineDateLessThanOrEqualToInputDate(l9, inputDate));
		Assertions.assertTrue(HelperService.isLineDateLessThanOrEqualToInputDate(l6, inputDate));
		Assertions.assertFalse(HelperService.isLineDateLessThanOrEqualToInputDate(l1, inputDate));
	}

	@Test
	void isNonExistentDateTest() {
		LocalDate inputDate = UtilitiesService.getParsedLocalDateTime("2018-12-08T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertFalse(HelperService.isNonExistentDate(l5, l6, inputDate));
		Assertions.assertTrue(HelperService.isNonExistentDate("a,2018-12-09T22:05:00+00:00", "b,2018-12-07T22:05:00+00:00", inputDate));
		Assertions.assertFalse(HelperService.isNonExistentDate("a,2018-12-10T22:05:00+00:00", "b,2018-12-09T22:05:00+00:00", inputDate));
	}

	@Test
	void getLeftEndIndexTest() {
		Assertions.assertEquals(3, HelperService.getLeftEndIndex(1, 5));
		Assertions.assertEquals(5, HelperService.getLeftEndIndex(1, 10));
		Assertions.assertEquals(7, HelperService.getLeftEndIndex(5, 10));
		Assertions.assertEquals(7, HelperService.getLeftEndIndex(4, 10));
	}

	@Test
	void isInputDateBeyondBoundaryOfFileTest() {
		LocalDate validInputDate = UtilitiesService.getParsedLocalDateTime("2018-12-08T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		LocalDate invalidInputDate = UtilitiesService.getParsedLocalDateTime("2018-12-10T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertFalse(HelperService.isInputDateBeyondBoundaryOfFile("Some/Path", validInputDate, 8));
		Assertions.assertTrue(HelperService.isInputDateBeyondBoundaryOfFile("Some/Path", invalidInputDate, 9));
		invalidInputDate = UtilitiesService.getParsedLocalDateTime("2018-12-01T22:03:00+00:00",
				DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate();
		Assertions.assertTrue(HelperService.isInputDateBeyondBoundaryOfFile("Some/Path", invalidInputDate, 9));
	}

	@Test
	void getLinesFromFileTest() {
		Assertions.assertEquals(fileContent.size(), HelperService.getLinesFromFile("sone/path", false).count());
		Assertions.assertEquals(fileContent.size() - 1, HelperService.getLinesFromFile("sone/path", true).count());
	}

	@Test
	void getLinesFromFileTest_IOException() {
		msUs.when(() -> UtilitiesService.getFileLines(anyString())).thenThrow(IOException.class);
		Assertions.assertEquals(0, HelperService.getLinesFromFile("sone/path", true).count());
	}

	@Test
	void getLinesFromFileTest_FileWithOnlyHeader() {
		msUs.when(() -> UtilitiesService.getFileLines(anyString())).thenReturn(Stream.of(l1));
		Assertions.assertEquals(0, HelperService.getLinesFromFile("sone/path", true).count());
	}
}
