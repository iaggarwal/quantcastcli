package org.ishan.quantcast.validators;

import org.ishan.quantcast.services.UtilitiesService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class DateValidator implements ConstraintValidator<CheckValidDate, String> {

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
		if (Optional.ofNullable(object).orElse("").isEmpty()) {
			return true;
		}
		try {
			return Optional.ofNullable(UtilitiesService.getParsedLocalDate(object, DateTimeFormatter.ISO_LOCAL_DATE)).isPresent();
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}


}
