package org.ishan.quantcast.validators;

import org.apache.commons.io.FilenameUtils;
import org.ishan.quantcast.services.UtilitiesService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.File;
import java.util.Optional;

public class FilePathValidator implements ConstraintValidator<CheckValidFilePath, String> {
	@Override
	public boolean isValid(String filePath, ConstraintValidatorContext constraintValidatorContext) {
		if (Optional.ofNullable(filePath).orElse("").isEmpty()) {
			return true;
		}
		File file = UtilitiesService.getFile(filePath);
		return file.exists() && FilenameUtils.isExtension(filePath, "csv");
	}
}
