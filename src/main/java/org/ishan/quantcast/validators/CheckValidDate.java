package org.ishan.quantcast.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.format.DateTimeFormatter;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = DateValidator.class)
public @interface CheckValidDate {
	String message() default "Error: Invalid date format or Invalid date. Please enter a valid date in format YYYY-MM-DD";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
