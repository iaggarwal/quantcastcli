package org.ishan.quantcast.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CookieFileService {

	private CookieFileService() {}

	public static Set<String> getCookiesWithMaxOccurrences(String filePath, String date) {
		long count = HelperService.getNumberOfLinesInFile(filePath, true);
		if (count == 0) {
			System.out.println("Error: Empty file!");
			return new HashSet<>();
		}
		LocalDate inputDate = UtilitiesService.getParsedLocalDate(date, DateTimeFormatter.ISO_LOCAL_DATE);
		if (HelperService.isInputDateBeyondBoundaryOfFile(filePath, inputDate, count)) {
			System.out.println("Error: Input date outside file boundary");
			return new HashSet<>();
		}
		long start = CookieFileService.getStartIndex(filePath, 1, count, inputDate);
		return parseChunk(filePath, start, inputDate);
	}

	private static long getStartIndex(String filePath, long start, long end, LocalDate inputDate) {
		if (start == end) {
			String line = HelperService.getNthLineFromFile(filePath, true, start);
			if (HelperService.isLineDateSameAsInputDate(line, inputDate)) {
				return start;
			} else {
				return -1;
			}
		}
		long leftEnd = HelperService.getLeftEndIndex(start, end);
		long rightStart = leftEnd + 1;
		String leftEndLine = HelperService.getNthLineFromFile(filePath, true, leftEnd);
		String rightStartLine = HelperService.getNthLineFromFile(filePath, true, rightStart);

		if (HelperService.isLineDateLessThanOrEqualToInputDate(leftEndLine, inputDate)) {
			return getStartIndex(filePath, start, leftEnd, inputDate);
		} else if (HelperService.isLineDateGreaterThanInputDate(rightStartLine, inputDate)) {
			return getStartIndex(filePath, rightStart, end, inputDate);
		} else if(HelperService.isNonExistentDate(leftEndLine, rightStartLine, inputDate)) {
			return -1;
		} else {
			return rightStart;
		}
	}

	private static Set<String> parseChunk(String filePath, long firstIndex, LocalDate inputDate) {
		CookieService cs = new CookieService();
		if (firstIndex < 0) {
			return new HashSet<>();
		}
		HelperService.getLinesFromFile(filePath, true)
				.skip(firstIndex - 1)
				.filter(HelperService::isValidLine)
				.anyMatch(l -> {
					String[] split = l.split(",");
					String cookie = split[0];
					return HelperService.getDateFromLine(l).map(ld -> {
						if (ld.isEqual(inputDate)) {
							cs.addCookie(cookie);
						}
						return !ld.isEqual(inputDate);
					}).orElse(false);
				});
		return cs.getCookiesWithMaxCount();
	}

}
