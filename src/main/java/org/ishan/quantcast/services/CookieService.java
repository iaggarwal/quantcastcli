package org.ishan.quantcast.services;

import java.util.*;

public class CookieService {

	private final Map<String, Integer> cache;
	private final LinkedList<String> cookieList;
	Integer maxCount = 0;

	public CookieService() {
		cache = new HashMap<>();
		cookieList = new LinkedList<>();
	}

	public void addCookie(String cookie) {
		cache.put(cookie, cache.getOrDefault(cookie, 0) + 1);
		if (cache.get(cookie) > maxCount) {
			maxCount = cache.get(cookie);
			cookieList.removeFirstOccurrence(cookie);
			cookieList.addFirst(cookie);
		} else if (cache.get(cookie).equals(maxCount)) {
			cookieList.removeFirstOccurrence(cookie);
			cookieList.addFirst(cookie);
		} else {
			cookieList.removeFirstOccurrence(cookie);
		}
	}

	public Set<String> getCookiesWithMaxCount() {
		Set<String> cList = new HashSet<>();
		for (int i=0 ; i < cookieList.size() && cache.get(cookieList.get(i)).equals(maxCount) ; i++) {
			cList.add(cookieList.get(i));
		}
		return cList;
	}
}
