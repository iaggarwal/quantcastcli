package org.ishan.quantcast.services;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

// Non unit-testable methods
public class UtilitiesService {

	public static File getFile(String path) {
		return new File(path);
	}

	public static Stream<String> getFileLines(String path) throws IOException {
		return Files.lines(Paths.get(path));
	}

	public static LocalDateTime getParsedLocalDateTime(String date, DateTimeFormatter pattern) {
		return LocalDateTime.parse(date, pattern);
	}

	public static LocalDate getParsedLocalDate(String date, DateTimeFormatter pattern) {
		return LocalDate.parse(date, pattern);
	}

	public static Validator getValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		return factory.getValidator();
	}
}
