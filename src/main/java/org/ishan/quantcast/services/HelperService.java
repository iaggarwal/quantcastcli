package org.ishan.quantcast.services;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.stream.Stream;

public class HelperService {

	private HelperService() {}

	public static boolean isValidLine(String line) {
		return line != null && !line.isEmpty() && line.split(",").length == 2;
	}

	public static String getNthLineFromFile(String path, boolean header, long n) {
		if (n == 1) {
			return getLinesFromFile(path, header)
					.filter(HelperService::isValidLine)
					.findFirst()
					.orElse("");
		} else {
			return getLinesFromFile(path, header).skip(n - 1)
					.filter(HelperService::isValidLine)
					.findFirst()
					.orElse("");
		}
	}

	//TODO: Check if there is a better approach
	public static Long getNumberOfLinesInFile(String path, boolean header) {
		return Optional.ofNullable(getLinesFromFile(path, header)).map(Stream::count).orElse(0L);
	}

	static long getLeftEndIndex(long start, long end) {
		long numberOfLines = end - start + 1;
		if ((numberOfLines) % 2 == 0) {
			return start + (numberOfLines) / 2 - 1;
		} else {
			return start + (long) Math.ceil((double)numberOfLines / 2) - 1;
		}
	}

	static boolean isNonExistentDate(String leftLine, String rightLine, LocalDate inputDate) {
		return HelperService.getDateFromLine(leftLine).map(inputDate::isBefore).orElse(false)
				&& HelperService.getDateFromLine(rightLine).map(inputDate::isAfter).orElse(false);
	}

	static boolean isLineDateLessThanOrEqualToInputDate(String line, LocalDate inputDate) {
		return HelperService.getDateFromLine(line).map(inputDate::isEqual).orElse(false)
				|| HelperService.getDateFromLine(line).map(inputDate::isAfter).orElse(false);
	}

	static boolean isLineDateGreaterThanInputDate(String line, LocalDate inputDate) {
		return HelperService.getDateFromLine(line).map(inputDate::isBefore).orElse(false);
	}

	static boolean isLineDateSameAsInputDate(String line, LocalDate inputDate) {
		return HelperService.getDateFromLine(line).map(inputDate::isEqual).orElse(false);
	}

	static Optional<LocalDate> getDateFromLine(String line) {
		String[] splitLine = Optional.ofNullable(line).orElse("").split(",");
		if (splitLine.length != 2) {
			return Optional.empty();
		}
		String date = line.split(",")[1];
		try {
			LocalDateTime d = UtilitiesService.getParsedLocalDateTime(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			return Optional.ofNullable(d.toLocalDate());
		} catch (DateTimeParseException e) {
			return Optional.empty();
		}
	}

	static boolean isInputDateBeyondBoundaryOfFile(String filePath, LocalDate inputDate, long count) {
		Optional<String> firstLine = getLinesFromFile(filePath, true).findFirst();
		Optional<String> lastLine = getLinesFromFile(filePath, true).skip(count - 1).findFirst();
		boolean isGreaterThanFirstDate = firstLine
				.map(fl -> getDateFromLine(fl).map(d -> d.isBefore(inputDate)).orElse(true))
				.orElse(true);
		boolean isLessThanLastDate = lastLine
				.map(fl -> getDateFromLine(fl).map(d -> d.isAfter(inputDate)).orElse(true))
				.orElse(true);
		return isGreaterThanFirstDate || isLessThanLastDate;
	}

	public static Stream<String> getLinesFromFile(String path, boolean header) {
		Stream<String> lines;
		try {
			lines = UtilitiesService.getFileLines(path);
		} catch (IOException ioException) {
			return Stream.empty();
		}
		return header ? lines.skip(1) : lines;
	}
}
