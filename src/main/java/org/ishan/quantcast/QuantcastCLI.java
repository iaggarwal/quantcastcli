package org.ishan.quantcast;

import org.ishan.quantcast.services.UtilitiesService;
import picocli.CommandLine;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public abstract class QuantcastCLI implements IQuantcastCLI {

	public boolean validate(CommandLine.Model.CommandSpec spec) {
		Validator validator = UtilitiesService.getValidator();
		Set<ConstraintViolation<QuantcastCLI>> violations = validator.validate(this);

		if (!violations.isEmpty()) {
			String errorMsg = "";
			for (ConstraintViolation<QuantcastCLI> violation : violations) {
				errorMsg += "ERROR: " + violation.getMessage() + "\n";
			}
			throw new CommandLine.ParameterException(spec.commandLine(), errorMsg);
		}

		return true;
	}
}
