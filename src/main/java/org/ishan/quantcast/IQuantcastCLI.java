package org.ishan.quantcast;

import picocli.CommandLine;

public interface IQuantcastCLI {
	boolean validate(CommandLine.Model.CommandSpec spec);
}
