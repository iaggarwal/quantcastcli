package org.ishan.quantcast;

import org.ishan.quantcast.services.CookieFileService;
import org.ishan.quantcast.validators.CheckValidDate;
import org.ishan.quantcast.validators.CheckValidFilePath;
import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Spec;

import java.util.Set;
import java.util.concurrent.Callable;
import javax.validation.constraints.NotBlank;

public class CookieFileCLI extends QuantcastCLI implements Callable<Integer>, IQuantcastCLI {

	@Option(names = {"-f", "--file"}, description = "Path for cookie file(.csv format). First line is expected to be the header.")
	@NotBlank(message = "Option -f or --file cannot be null or empty")
	@CheckValidFilePath
	String filePath;

	@Option(names = {"-d", "--date"}, description = "Date for which max occurrence of same cookie is to be calculated. Date should be in format YYYY-MM-DD.")
	@NotBlank(message = "Option -d or --date cannot be null or empty")
	@CheckValidDate
	String date;

	@Spec
	CommandLine.Model.CommandSpec spec;

	public static void main(String[] args) {
		int exitCode = new CommandLine(new CookieFileCLI()).execute(args);
		System.exit(exitCode);
	}

	public Integer call() {
		validate(spec);
		Set<String> cookies = CookieFileService.getCookiesWithMaxOccurrences(filePath, date);
		cookies.forEach(System.out::println);
		return null;
	}
}
